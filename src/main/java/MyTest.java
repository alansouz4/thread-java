public class MyTest {
    static int i = 0;

    public static void main(String[] args) {
        new Thread(t1).start();
        new Thread(t2).start();
    }

    private static void countMe(String name) {
        i++;
        System.out.println("Inicio programa");
        System.out.println("O contador atual é: " + i + ", atualizado por: " + name);
        System.out.println("Fim do programa!");
    }

    private static Runnable t1 = new Runnable() {
        public void run() {
            try {
                for(int i=0; i<5; i++) {
                    countMe("tt1");
                }
            } catch (Exception e){}
        }
    };

    private static Runnable t2 = new Runnable() {
        public void run() {
            try{
                for(int i=0; i<5; i++){
                    countMe("tt2");
                }
            } catch (Exception e){}
        }
    };
}
